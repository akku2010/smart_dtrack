import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SpeedRepoPage } from './speed-repo';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  declarations: [
    SpeedRepoPage,
  ],
  imports: [
    IonicPageModule.forChild(SpeedRepoPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class SpeedRepoPageModule {}
